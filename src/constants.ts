const throwError = (msg: string) => {
  throw new Error(msg)
}

export const UNSPLASH_ACCESS_KEY =
  process.env.REACT_APP_UNSPLASH_ACCESS_KEY ||
  throwError('REACT_APP_UNSPLASH_ACCESS_KEY is not defined')
