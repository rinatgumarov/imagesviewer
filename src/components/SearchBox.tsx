import React, {FC, useState} from 'react'

type SearchPropsType = {
  onSubmit: (query: string) => void
}

export const SearchBox: FC<SearchPropsType> = ({onSubmit}) => {
  const [query, setQuery] = useState('')

  return (
    <div className='ui segment'>
      <form
        className='ui form'
        onSubmit={(event) => {
          event.preventDefault()
          onSubmit(query)
        }}
      >
        <div className='field'>
          <label>Images Search</label>
          <input
            type='text'
            value={query}
            onChange={(event) => {
              setQuery(event.target.value)
            }}
          ></input>
        </div>
      </form>
    </div>
  )
}
