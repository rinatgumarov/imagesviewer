import axios from 'axios'
import {UNSPLASH_ACCESS_KEY} from '../constants'
export const unsplashApi = axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: `Client-ID ${UNSPLASH_ACCESS_KEY}`,
  },
})
