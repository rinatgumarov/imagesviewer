import {Enum} from '@martin_hotell/rex-tils'
import {Dispatch} from 'redux'
import {unsplashApi} from './api/unsplashApi'
import {Image} from './components/ImageCard'

const initialState = {
  query: '',
  page: 1,
  images: [] as Image[],
  loading: false,
}

export type State = typeof initialState

export const ActionType = Enum(
  'LOAD_IMAGES',
  'FETCH_IMAGES',
  'FETCH_MORE_IMAGES',
)

export type ActionType = Enum<typeof ActionType>

export const loadImages = () => ({
  type: ActionType.LOAD_IMAGES,
})

export const fetchImages = (query: string) => async (
  dispatch: Dispatch,
  getState: () => State,
) => {
  dispatch({type: ActionType.LOAD_IMAGES})
  const images = (await (
    await unsplashApi.get('/search/photos', {
      params: {
        query,
      },
    })
  ).data.results) as Image[]
  return dispatch({
    type: ActionType.FETCH_IMAGES,
    query,
    images,
  })
}

export const fetchMoreImages = () => async (
  dispatch: Dispatch,
  getState: () => State,
) => {
  const {query, page} = getState()
  dispatch({type: ActionType.LOAD_IMAGES})
  const images = (await (
    await unsplashApi.get('/search/photos', {
      params: {
        query,
        page: page + 1,
      },
    })
  ).data.results) as Image[]
  return dispatch({
    type: ActionType.FETCH_MORE_IMAGES,
    images,
  })
}
type ThenArg<T> = T extends Promise<infer U>
  ? U
  : T extends (...args: any[]) => Promise<infer V>
  ? V
  : T
type FetchMoreImagesAction = ThenArg<ReturnType<typeof fetchMoreImages>>
type FetchImagesAction = ThenArg<ReturnType<typeof fetchImages>>
type LoadImagesAction = ThenArg<ReturnType<typeof loadImages>>

export type Action =
  | FetchImagesAction
  | FetchMoreImagesAction
  | LoadImagesAction

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.LOAD_IMAGES:
      return {...state, loading: true}
    case ActionType.FETCH_IMAGES:
      return {
        ...state,
        query: action.query,
        images: action.images,
        page: 1,
        loading: false,
      }
    case ActionType.FETCH_MORE_IMAGES:
      return {
        ...state,
        images: [...state.images, ...action.images],
        page: state.page + 1,
        loading: false,
      }
    default:
      return state
  }
}
