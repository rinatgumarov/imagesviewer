import React, {useCallback, useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {fetchImages, fetchMoreImages, State} from '../store'
import {ImageList} from './ImageList'
import {SearchBox} from './SearchBox'

export const App = () => {
  const dispatch = useDispatch()
  const loading = useSelector((state: State) => state.loading)

  const handleScroll = useCallback(async () => {
    const shouldFetchMore =
      !loading &&
      window.innerHeight + document.documentElement.scrollTop + 10 >
        document.documentElement.scrollHeight
    if (shouldFetchMore) {
      dispatch(fetchMoreImages())
    }
  }, [dispatch, loading])

  const onSubmit = useCallback(
    (query: string) => {
      dispatch(fetchImages(query))
    },
    [dispatch],
  )

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => window.removeEventListener('scroll', handleScroll)
  }, [handleScroll])

  return (
    <div className='ui container' style={{marginTop: 10}}>
      <SearchBox onSubmit={onSubmit}></SearchBox>
      <ImageList />
    </div>
  )
}
