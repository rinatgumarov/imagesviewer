import React, {FC} from 'react'
import {useSelector} from 'react-redux'
import {State} from '../store'
import {ImageCard} from './ImageCard'
import './ImageList.css'

export const ImageList: FC = () => {
  const images = useSelector((state: State) => [...state.images])
  return (
    <div className='image-list'>
      {images.map((image) => (
        <ImageCard key={image.id} image={image} />
      ))}
    </div>
  )
}
