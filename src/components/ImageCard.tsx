import React, {FC, useCallback, useLayoutEffect, useRef, useState} from 'react'

export type Image = {
  urls: {regular: string}
  id: string
  description: string
}

export const ImageCard: FC<{image: Image}> = ({image}) => {
  const [spans, setSpans] = useState(0)
  const [visible, setVisible] = useState(false)
  const imageRef = useRef<HTMLImageElement>(null)
  const onImageLoad = useCallback(() => {
    setSpans(Math.ceil((imageRef.current?.clientHeight || 0) / 10))
    setVisible(true)
  }, [])
  useLayoutEffect(() => {
    imageRef.current?.addEventListener('load', onImageLoad)
  }, [onImageLoad])
  return (
    <div
      style={{
        gridRowEnd: `span ${spans}`,
        visibility: visible ? 'visible' : 'hidden',
      }}
    >
      <img
        ref={imageRef}
        // onLoad={onImageLoad}
        src={image.urls.regular}
        alt={image.description}
      ></img>
    </div>
  )
}
